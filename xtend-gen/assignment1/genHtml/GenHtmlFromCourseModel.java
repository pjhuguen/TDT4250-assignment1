package assignment1.genHtml;

import assignment1.Assignment1Package;
import assignment1.Course;
import assignment1.CourseInstance;
import assignment1.Day;
import assignment1.Department;
import assignment1.HourType;
import assignment1.Role;
import assignment1.ScheduledHour;
import assignment1.Semester;
import assignment1.Work;
import assignment1.WorkType;
import assignment1.util.Assignment1ResourceFactoryImpl;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class GenHtmlFromCourseModel {
  /**
   * TODO
   * - Avoir l'exemple sur un seul file, ce sera plus facile, et l'avoir dans le meme package que ce fichier
   * - Recopier la fonction maindu "quiz2TextGenerator"
   * - Le fichier quiz2TextGenerator est le seul qui nous interesse, en gros on va construire petit � petit le texte de notre page HTML, on fait juste une onepage
   * - Pas besoin de script car la page ne doit pas �tre int�ractive
   * - Faut faire un petit tableau pour les horaires
   * - Petite subtilit� chez lui, car son TextGen et XHtmlGen partagent des choses
   * - TextGen et XHtmlGen font la m�me chose mais de mani�re diff�rentes, Text cr�e du texte pour notre html (avec les balises et tout) alors que XHtml cr�e un objet html
   * - On peut ajouter des bouts de textes html n'importe ou dans notre page (appeller des fonctions dans le texte apres les ''' )
   * - On ne touche pas au dossier xtend-gen, il se g�re tout seul
   * - Normalement on a le texte html qui saffiche dans la console, on peut juste le copier-coller dans notre fichier .html pour voir notre page
   * - Pour �crire directement sur le fichier .html, il faut bidouiller les arguments dans le "click droit -> Run configuration -> arguments"
   * - Le fichier XhtmlUtil.xtend peut �tre utile, a voir
   * 
   * -On fait tout dans cette classe genHtmlFromCourseModel
   * - Des petites fonctions pour chaque bloc html :)
   */
  public String generateHtml(final Course course) {
    StringBuilder _stringBuilder = new StringBuilder();
    return this.generateHtml(course, _stringBuilder).toString();
  }
  
  public CharSequence generateHtml(final Course course, final StringBuilder stringBuilder) {
    CharSequence _xblockexpression = null;
    {
      this.generatePreHtml(course.getCode(), course.getName(), stringBuilder);
      this.generateContent(course, stringBuilder);
      _xblockexpression = this.generatePostHtml(stringBuilder);
    }
    return _xblockexpression;
  }
  
  public CharSequence generatePreHtml(final String code, final String name, final StringBuilder stringBuilder) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<!DOCTYPE html>");
    _builder.newLine();
    _builder.append("<html>");
    _builder.newLine();
    _builder.append("<head>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<title>");
    _builder.append(code, "\t");
    _builder.append(" - ");
    _builder.append(name, "\t");
    _builder.append("\"</title>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("<meta charset=\"utf-8\"/>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<style>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("table, th, td {");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("border: 1px solid black;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</style>\t");
    _builder.newLine();
    _builder.append("</head>");
    _builder.newLine();
    _builder.append("<body>");
    _builder.newLine();
    return this.operator_doubleLessThan(stringBuilder, _builder);
  }
  
  public CharSequence generatePostHtml(final StringBuilder stringBuilder) {
    return this.operator_doubleLessThan(stringBuilder, "\n</body></html>");
  }
  
  public CharSequence generateContent(final Course course, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h1>");
      String _code = course.getCode();
      _builder.append(_code);
      _builder.append(" - ");
      String _name = course.getName();
      _builder.append(_name);
      _builder.append("</h1>");
      _builder.newLineIfNotEmpty();
      _builder.append("<h2>Content</h2>");
      _builder.newLine();
      String _content = course.getContent();
      _builder.append(_content);
      _builder.newLineIfNotEmpty();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<CourseInstance> _instances = course.getInstances();
      for (final CourseInstance ci : _instances) {
        {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("<h2>");
          Semester _semester = ci.getSemester();
          _builder_1.append(_semester);
          _builder_1.append(" - ");
          int _year = ci.getYear();
          _builder_1.append(_year);
          _builder_1.append("</h2>  ");
          _builder_1.newLineIfNotEmpty();
          this.operator_doubleLessThan(stringBuilder, _builder_1);
          this.generateEvaluationForm(ci, stringBuilder);
          this.generateTimetable(ci, stringBuilder);
          this.generateStaff(ci, stringBuilder);
        }
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_1);
    }
    return _xblockexpression;
  }
  
  public CharSequence generateEvaluationForm(final CourseInstance courseInstance, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h3>Examination arrangement</h3>");
      _builder.newLine();
      _builder.append("<table>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("<tr>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Evaluation Form</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Weighting</th>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("</tr>");
      _builder.newLine();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<Work> _evaluationForm = courseInstance.getEvaluationForm();
      for (final Work w : _evaluationForm) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("<tr>");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        WorkType _type = w.getType();
        _builder_1.append(_type, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        int _percentage = w.getPercentage();
        _builder_1.append(_percentage, "\t");
        _builder_1.append("/100</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("</tr>");
        _builder_1.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("</table>");
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_2);
    }
    return _xblockexpression;
  }
  
  public CharSequence generateTimetable(final CourseInstance courseInstance, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h3>Timetable</h3>");
      _builder.newLine();
      _builder.append("<table>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("<tr>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Day</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Type</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Room</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Time</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Length</th>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("</tr>");
      _builder.newLine();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<ScheduledHour> _labHours = courseInstance.getLabHours();
      for (final ScheduledHour lh : _labHours) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("<tr>");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        Day _day = lh.getDay();
        _builder_1.append(_day, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        HourType _type = lh.getType();
        _builder_1.append(_type, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _room = lh.getRoom();
        _builder_1.append(_room, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _beginning = lh.getBeginning();
        _builder_1.append(_beginning, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        int _duration = lh.getDuration();
        _builder_1.append(_duration, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("</tr>");
        _builder_1.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_1);
      }
      EList<ScheduledHour> _lectureHours = courseInstance.getLectureHours();
      for (final ScheduledHour lh_1 : _lectureHours) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("<tr>");
        _builder_2.newLine();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        Day _day_1 = lh_1.getDay();
        _builder_2.append(_day_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        HourType _type_1 = lh_1.getType();
        _builder_2.append(_type_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        String _room_1 = lh_1.getRoom();
        _builder_2.append(_room_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        String _beginning_1 = lh_1.getBeginning();
        _builder_2.append(_beginning_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("<td>");
        int _duration_1 = lh_1.getDuration();
        _builder_2.append(_duration_1, "\t");
        _builder_2.append("</td>");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("</tr>");
        _builder_2.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_2);
      }
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("</table>");
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_3);
    }
    return _xblockexpression;
  }
  
  public CharSequence generateStaff(final CourseInstance courseInstance, final StringBuilder stringBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<h3>Contact information</h3>");
      _builder.newLine();
      _builder.append("Department with academic responsibility : ");
      _builder.newLine();
      String _name = courseInstance.getCourse().getDepartment().getName();
      _builder.append(_name);
      _builder.newLineIfNotEmpty();
      _builder.append("<table>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("<tr>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Name</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Email</th>");
      _builder.newLine();
      _builder.append("           ");
      _builder.append("<th>Role</th>");
      _builder.newLine();
      _builder.append("       ");
      _builder.append("</tr>");
      _builder.newLine();
      this.operator_doubleLessThan(stringBuilder, _builder);
      EList<Role> _staff = courseInstance.getStaff();
      for (final Role r : _staff) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("<tr>");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _name_1 = r.getPeople().getName();
        _builder_1.append(_name_1, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _email = r.getPeople().getEmail();
        _builder_1.append(_email, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("<td>");
        String _type = r.getType();
        _builder_1.append(_type, "\t");
        _builder_1.append("</td>");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("</tr>");
        _builder_1.newLine();
        this.operator_doubleLessThan(stringBuilder, _builder_1);
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("</table>");
      _xblockexpression = this.operator_doubleLessThan(stringBuilder, _builder_2);
    }
    return _xblockexpression;
  }
  
  public StringBuilder operator_doubleLessThan(final StringBuilder stringBuilder, final Object o) {
    return stringBuilder.append(o);
  }
  
  public static void main(final String[] args) throws IOException {
    final List<String> argsAsList = Arrays.<String>asList(args);
    Course _xifexpression = null;
    int _size = argsAsList.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      _xifexpression = GenHtmlFromCourseModel.getCourse(argsAsList.get(0));
    } else {
      _xifexpression = GenHtmlFromCourseModel.getSampleCourse();
    }
    final Course course = _xifexpression;
    final String html = new GenHtmlFromCourseModel().generateHtml(course);
    int _length = args.length;
    boolean _greaterThan_1 = (_length > 1);
    if (_greaterThan_1) {
      final URI target = URI.createURI(argsAsList.get(1));
      OutputStream _createOutputStream = course.eResource().getResourceSet().getURIConverter().createOutputStream(target);
      final PrintStream ps = new PrintStream(_createOutputStream);
      ps.print(html);
    } else {
      System.out.println(html);
    }
  }
  
  public static Course getCourse(final String uriString) throws IOException {
    final ResourceSetImpl resSet = new ResourceSetImpl();
    resSet.getPackageRegistry().put(Assignment1Package.eNS_URI, Assignment1Package.eINSTANCE);
    Map<String, Object> _extensionToFactoryMap = resSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
    Assignment1ResourceFactoryImpl _assignment1ResourceFactoryImpl = new Assignment1ResourceFactoryImpl();
    _extensionToFactoryMap.put("xmi", _assignment1ResourceFactoryImpl);
    final Resource resource = resSet.getResource(URI.createURI(uriString), true);
    EList<EObject> _contents = resource.getContents();
    for (final EObject eObject : _contents) {
      if ((eObject instanceof Department)) {
        return ((Department)eObject).getCourses().get(0);
      }
    }
    return null;
  }
  
  public static Course getSampleCourse() {
    try {
      return GenHtmlFromCourseModel.getCourse(GenHtmlFromCourseModel.class.getResource("sample.xmi").toString());
    } catch (final Throwable _t) {
      if (_t instanceof IOException) {
        final IOException e = (IOException)_t;
        System.err.println(e);
        return null;
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
}
