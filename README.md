# Executing the runnable
The file to execute is `GenHtmlFromCourseModel.xtend`. You can either execute it without any parameters, which will result in transforming the sample instance (sample.xmi) and writing the result on the standard output. If you provide one parameter, this parameter should specify the instance file to transform. You can also provide a second parameter which will specify the output file.

# Description of the main classes
## Course
Class that models a course with its code, name, content and contains instances for each semester where this course is taught. It has a list of CourseRelation classes, which represent the number of credits deducted if you have previously taken a certain course.

## CourseInstance
Class that contains all the information for a course, specific to a semester. It contains the staff, timetable and work required.

## ScheduledHours
Abstract class that models a scheduled hour for a course. It can be either a lab or a lecture hour and it contains all information needed to define the timetable, especially the study programs they are reserved for (if any).

## Work 
Abstract class that models a required work for a course. If the percentage is set to 0 then it doesn't count in the final grade.

## Role
Class that models a role specific to a course. It has a people and a type, and each course must have a role with "coordinator" as a type.
