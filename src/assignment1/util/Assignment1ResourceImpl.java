/**
 */
package assignment1.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see Assignment1ResourceFactoryImpl.hal.quiz.util.QuizResourceFactoryImpl
 * @generated
 */
public class Assignment1ResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Assignment1ResourceImpl(URI uri) {
		super(uri);
	}

} //QuizResourceImpl
