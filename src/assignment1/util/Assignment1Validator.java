/**
 */
package assignment1.util;

import assignment1.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see assignment1.Assignment1Package
 * @generated
 */
public class Assignment1Validator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Assignment1Validator INSTANCE = new Assignment1Validator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "assignment1";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignment1Validator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Assignment1Package.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Assignment1Package.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case Assignment1Package.COURSE_RELATION:
				return validateCourseRelation((CourseRelation)value, diagnostics, context);
			case Assignment1Package.COURSE_INSTANCE:
				return validateCourseInstance((CourseInstance)value, diagnostics, context);
			case Assignment1Package.COURSE_WORK:
				return validateCourseWork((CourseWork)value, diagnostics, context);
			case Assignment1Package.ROLE:
				return validateRole((Role)value, diagnostics, context);
			case Assignment1Package.PEOPLE:
				return validatePeople((People)value, diagnostics, context);
			case Assignment1Package.STUDY_PROGRAM:
				return validateStudyProgram((StudyProgram)value, diagnostics, context);
			case Assignment1Package.WORK:
				return validateWork((Work)value, diagnostics, context);
			case Assignment1Package.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case Assignment1Package.SCHEDULED_HOUR:
				return validateScheduledHour((ScheduledHour)value, diagnostics, context);
			case Assignment1Package.SEMESTER:
				return validateSemester((Semester)value, diagnostics, context);
			case Assignment1Package.DAY:
				return validateDay((Day)value, diagnostics, context);
			case Assignment1Package.WORK_TYPE:
				return validateWorkType((WorkType)value, diagnostics, context);
			case Assignment1Package.HOUR_TYPE:
				return validateHourType((HourType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseRelation(CourseRelation courseRelation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(courseRelation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_percentageSum(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_hasACoordinator(courseInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourseInstance_timetableCorrespondsToCoursework(courseInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the percentageSum constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__PERCENTAGE_SUM__EEXPRESSION = "self.evaluationForm.percentage->sum() == 100";

	/**
	 * Validates the percentageSum constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_percentageSum(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Assignment1Package.Literals.COURSE_INSTANCE,
				 courseInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "percentageSum",
				 COURSE_INSTANCE__PERCENTAGE_SUM__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the hasACoordinator constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE_INSTANCE__HAS_ACOORDINATOR__EEXPRESSION = "self.staff.type->select(item | item == \"coordinator\")->length == 1";

	/**
	 * Validates the hasACoordinator constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance_hasACoordinator(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Assignment1Package.Literals.COURSE_INSTANCE,
				 courseInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "hasACoordinator",
				 COURSE_INSTANCE__HAS_ACOORDINATOR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Validates the timetableCorrespondsToCoursework constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_timetableCorrespondsToCoursework(CourseInstance courseInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		int lectureHoursNeeded = courseInstance.getCourseWork().getLectureHours();
		int labHoursNeeded = courseInstance.getCourseWork().getLabHours();
		EList<ScheduledHour> labHoursScheduled = courseInstance.getLabHours();
		EList<ScheduledHour> lectureHoursScheduled = courseInstance.getLectureHours();
		EList<StudyProgram> l = courseInstance.getStudyPrograms();
		for (StudyProgram program : l) {
			int labHourPerProgram = 0;
			for (ScheduledHour labHour : labHoursScheduled) {
				if (labHour.getReservedForProgram().contains(program) || labHour.getReservedForProgram() == null) {
					labHourPerProgram += labHour.getDuration();
				}
			}
			int lectureHourPerProgram = 0;
			for (ScheduledHour lectureHour : lectureHoursScheduled) {
				if (lectureHour.getReservedForProgram().contains(program)
						|| lectureHour.getReservedForProgram() == null) {
					lectureHourPerProgram += lectureHour.getDuration();
				}
			}
			if (labHourPerProgram != labHoursNeeded || lectureHourPerProgram != lectureHoursNeeded) {
				if (diagnostics != null) {
					diagnostics.add(
							createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
									new Object[] { "timetableCorrespondsToCoursework",
											getObjectLabel(courseInstance, context) },
									new Object[] { courseInstance }, context));
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseWork(CourseWork courseWork, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(courseWork, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRole(Role role, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(role, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePeople(People people, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(people, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyProgram(StudyProgram studyProgram, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(studyProgram, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWork(Work work, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(work, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScheduledHour(ScheduledHour scheduledHour, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(scheduledHour, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemester(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDay(Day day, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWorkType(WorkType workType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHourType(HourType hourType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Assignment1Validator
