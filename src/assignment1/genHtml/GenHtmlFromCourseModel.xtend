package assignment1.genHtml

import assignment1.Course
import java.io.IOException
import java.util.Arrays
import org.eclipse.emf.common.util.URI
import java.io.PrintStream
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.EObject
import assignment1.Assignment1Package
import assignment1.util.Assignment1ResourceFactoryImpl
import assignment1.Department
import assignment1.CourseInstance
import assignment1.Work
import assignment1.ScheduledHour
import assignment1.Role

class GenHtmlFromCourseModel {
	/*
	 * TODO
	 * - Avoir l'exemple sur un seul file, ce sera plus facile, et l'avoir dans le meme package que ce fichier
	 * - Recopier la fonction maindu "quiz2TextGenerator"
	 * - Le fichier quiz2TextGenerator est le seul qui nous interesse, en gros on va construire petit � petit le texte de notre page HTML, on fait juste une onepage
	 * - Pas besoin de script car la page ne doit pas �tre int�ractive
	 * - Faut faire un petit tableau pour les horaires
	 * - Petite subtilit� chez lui, car son TextGen et XHtmlGen partagent des choses
	 * - TextGen et XHtmlGen font la m�me chose mais de mani�re diff�rentes, Text cr�e du texte pour notre html (avec les balises et tout) alors que XHtml cr�e un objet html
	 * - On peut ajouter des bouts de textes html n'importe ou dans notre page (appeller des fonctions dans le texte apres les ''' )
	 * - On ne touche pas au dossier xtend-gen, il se g�re tout seul
	 * - Normalement on a le texte html qui saffiche dans la console, on peut juste le copier-coller dans notre fichier .html pour voir notre page
	 * - Pour �crire directement sur le fichier .html, il faut bidouiller les arguments dans le "click droit -> Run configuration -> arguments"
	 * - Le fichier XhtmlUtil.xtend peut �tre utile, a voir
	 * 
	 * -On fait tout dans cette classe genHtmlFromCourseModel
	 * - Des petites fonctions pour chaque bloc html :)
	 * 
	 */
	def String generateHtml(Course course) {
		generateHtml(course, new StringBuilder).toString
	}

	def CharSequence generateHtml(Course course, StringBuilder stringBuilder) {
		generatePreHtml(course.code, course.name, stringBuilder)
		generateContent(course, stringBuilder);
		generatePostHtml(stringBuilder)
	}

	def CharSequence generatePreHtml(String code, String name, StringBuilder stringBuilder) {
		stringBuilder << '''
<!DOCTYPE html>
<html>
<head>
	<title>�code� - �name�"</title>
	<meta charset="utf-8"/>
	<style>
	table, th, td {
	    border: 1px solid black;
	}
	</style>	
</head>
<body>
'''
	}

	def CharSequence generatePostHtml(StringBuilder stringBuilder) {
		stringBuilder << "\n</body></html>"
	}

	def CharSequence generateContent(Course course, StringBuilder stringBuilder) {
		stringBuilder << '''
			<h1>�course.code� - �course.name�</h1>
			<h2>Content</h2>
			�course.content�
		'''
		for (CourseInstance ci : course.instances) {
			stringBuilder << '''
				<h2>�ci.semester� - �ci.year�</h2>  
			'''
			generateEvaluationForm(ci, stringBuilder);
			generateTimetable(ci, stringBuilder);
			generateStaff(ci, stringBuilder);
		}
		stringBuilder << ''''''
	}

	def CharSequence generateEvaluationForm(CourseInstance courseInstance, StringBuilder stringBuilder) {
		stringBuilder << '''
			<h3>Examination arrangement</h3>
			<table>
			       <tr>
			           <th>Evaluation Form</th>
			           <th>Weighting</th>
			       </tr>
		'''
		for (Work w : courseInstance.evaluationForm) {
			stringBuilder << '''
				<tr>
					<td>�w.type�</td>
					<td>�w.percentage�/100</td>
				</tr>
			'''
		}
		stringBuilder << '''
		</table>'''
	}

	def CharSequence generateTimetable(CourseInstance courseInstance, StringBuilder stringBuilder) {
		stringBuilder << '''
			<h3>Timetable</h3>
			<table>
			       <tr>
			           <th>Day</th>
			           <th>Type</th>
			           <th>Room</th>
			           <th>Time</th>
			           <th>Length</th>
			       </tr>
		'''
		for (ScheduledHour lh : courseInstance.labHours) {
			stringBuilder << '''
				<tr>
					<td>�lh.day�</td>
					<td>�lh.type�</td>
					<td>�lh.room�</td>
					<td>�lh.beginning�</td>
					<td>�lh.duration�</td>
				</tr>
			'''
		}
		for (ScheduledHour lh : courseInstance.lectureHours) {
			stringBuilder << '''
				<tr>
					<td>�lh.day�</td>
					<td>�lh.type�</td>
					<td>�lh.room�</td>
					<td>�lh.beginning�</td>
					<td>�lh.duration�</td>
				</tr>
			'''
		}
		stringBuilder << '''
		</table>'''
	}

	def CharSequence generateStaff(CourseInstance courseInstance, StringBuilder stringBuilder) {
				stringBuilder << '''
			<h3>Contact information</h3>
			Department with academic responsibility : 
			�courseInstance.course.department.name�
			<table>
			       <tr>
			           <th>Name</th>
			           <th>Email</th>
			           <th>Role</th>
			       </tr>
		'''
		for (Role r : courseInstance.staff) {
			stringBuilder << '''
				<tr>
					<td>�r.people.name�</td>
					<td>�r.people.email�</td>
					<td>�r.type�</td>
				</tr>
			'''
		}
		stringBuilder << '''
		</table>'''
	}


	def StringBuilder operator_doubleLessThan(StringBuilder stringBuilder, Object o) {
		return stringBuilder.append(o);
	}

	def static void main(String[] args) throws IOException {
		val argsAsList = Arrays.asList(args)
		val course = if(argsAsList.size > 0) getCourse(argsAsList.get(0)) else getSampleCourse();
		val html = new GenHtmlFromCourseModel().generateHtml(course);
		if (args.length > 1) {
			val target = URI.createURI(argsAsList.get(1));
			val ps = new PrintStream(course.eResource().getResourceSet().getURIConverter().createOutputStream(target))
			ps.print(html);
		} else {
			System.out.println(html);
		}
	}

	def static Course getCourse(String uriString) throws IOException {
		val resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(Assignment1Package.eNS_URI, Assignment1Package.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new Assignment1ResourceFactoryImpl());
		val resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Department) {
				return eObject.courses.get(0);
			}
		}
		return null;
	}

	def static Course getSampleCourse() {
		try {
			return getCourse(GenHtmlFromCourseModel.getResource("sample.xmi").toString());
		} catch (IOException e) {
			System.err.println(e);
			return null;
		}
	}
}
