/**
 */
package assignment1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see assignment1.Assignment1Package#getAssignment()
 * @model
 * @generated
 */
public interface Assignment extends Work {
} // Assignment
