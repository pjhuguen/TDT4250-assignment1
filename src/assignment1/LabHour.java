/**
 */
package assignment1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lab Hour</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see assignment1.Assignment1Package#getLabHour()
 * @model
 * @generated
 */
public interface LabHour extends ScheduledHour {
} // LabHour
