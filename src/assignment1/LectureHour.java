/**
 */
package assignment1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lecture Hour</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see assignment1.Assignment1Package#getLectureHour()
 * @model
 * @generated
 */
public interface LectureHour extends ScheduledHour {
} // LectureHour
