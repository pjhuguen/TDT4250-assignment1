/**
 */
package assignment1.impl;

import assignment1.Assignment1Package;
import assignment1.Course;
import assignment1.CourseInstance;
import assignment1.CourseRelation;
import assignment1.Department;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link assignment1.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getCreditsNumber <em>Credits Number</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getInstances <em>Instances</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getRequiredCourses <em>Required Courses</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getRecommendedCourses <em>Recommended Courses</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getCreditsReduction <em>Credits Reduction</em>}</li>
 *   <li>{@link assignment1.impl.CourseImpl#getDepartment <em>Department</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCreditsNumber() <em>Credits Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsNumber()
	 * @generated
	 * @ordered
	 */
	protected static final float CREDITS_NUMBER_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getCreditsNumber() <em>Credits Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsNumber()
	 * @generated
	 * @ordered
	 */
	protected float creditsNumber = CREDITS_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstances() <em>Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> instances;

	/**
	 * The cached value of the '{@link #getRequiredCourses() <em>Required Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> requiredCourses;

	/**
	 * The cached value of the '{@link #getRecommendedCourses() <em>Recommended Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommendedCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> recommendedCourses;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCreditsReduction() <em>Credits Reduction</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsReduction()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseRelation> creditsReduction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Assignment1Package.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Assignment1Package.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Assignment1Package.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getCreditsNumber() {
		return creditsNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreditsNumber(float newCreditsNumber) {
		float oldCreditsNumber = creditsNumber;
		creditsNumber = newCreditsNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Assignment1Package.COURSE__CREDITS_NUMBER, oldCreditsNumber, creditsNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getInstances() {
		if (instances == null) {
			instances = new EObjectContainmentWithInverseEList<CourseInstance>(CourseInstance.class, this, Assignment1Package.COURSE__INSTANCES, Assignment1Package.COURSE_INSTANCE__COURSE);
		}
		return instances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRequiredCourses() {
		if (requiredCourses == null) {
			requiredCourses = new EObjectResolvingEList<Course>(Course.class, this, Assignment1Package.COURSE__REQUIRED_COURSES);
		}
		return requiredCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRecommendedCourses() {
		if (recommendedCourses == null) {
			recommendedCourses = new EObjectResolvingEList<Course>(Course.class, this, Assignment1Package.COURSE__RECOMMENDED_COURSES);
		}
		return recommendedCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Assignment1Package.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseRelation> getCreditsReduction() {
		if (creditsReduction == null) {
			creditsReduction = new EObjectContainmentEList<CourseRelation>(CourseRelation.class, this, Assignment1Package.COURSE__CREDITS_REDUCTION);
		}
		return creditsReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getDepartment() {
		if (eContainerFeatureID() != Assignment1Package.COURSE__DEPARTMENT) return null;
		return (Department)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDepartment(Department newDepartment, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDepartment, Assignment1Package.COURSE__DEPARTMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepartment(Department newDepartment) {
		if (newDepartment != eInternalContainer() || (eContainerFeatureID() != Assignment1Package.COURSE__DEPARTMENT && newDepartment != null)) {
			if (EcoreUtil.isAncestor(this, newDepartment))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDepartment != null)
				msgs = ((InternalEObject)newDepartment).eInverseAdd(this, Assignment1Package.DEPARTMENT__COURSES, Department.class, msgs);
			msgs = basicSetDepartment(newDepartment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Assignment1Package.COURSE__DEPARTMENT, newDepartment, newDepartment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Assignment1Package.COURSE__INSTANCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInstances()).basicAdd(otherEnd, msgs);
			case Assignment1Package.COURSE__DEPARTMENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDepartment((Department)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Assignment1Package.COURSE__INSTANCES:
				return ((InternalEList<?>)getInstances()).basicRemove(otherEnd, msgs);
			case Assignment1Package.COURSE__CREDITS_REDUCTION:
				return ((InternalEList<?>)getCreditsReduction()).basicRemove(otherEnd, msgs);
			case Assignment1Package.COURSE__DEPARTMENT:
				return basicSetDepartment(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Assignment1Package.COURSE__DEPARTMENT:
				return eInternalContainer().eInverseRemove(this, Assignment1Package.DEPARTMENT__COURSES, Department.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Assignment1Package.COURSE__NAME:
				return getName();
			case Assignment1Package.COURSE__CODE:
				return getCode();
			case Assignment1Package.COURSE__CREDITS_NUMBER:
				return getCreditsNumber();
			case Assignment1Package.COURSE__INSTANCES:
				return getInstances();
			case Assignment1Package.COURSE__REQUIRED_COURSES:
				return getRequiredCourses();
			case Assignment1Package.COURSE__RECOMMENDED_COURSES:
				return getRecommendedCourses();
			case Assignment1Package.COURSE__CONTENT:
				return getContent();
			case Assignment1Package.COURSE__CREDITS_REDUCTION:
				return getCreditsReduction();
			case Assignment1Package.COURSE__DEPARTMENT:
				return getDepartment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Assignment1Package.COURSE__NAME:
				setName((String)newValue);
				return;
			case Assignment1Package.COURSE__CODE:
				setCode((String)newValue);
				return;
			case Assignment1Package.COURSE__CREDITS_NUMBER:
				setCreditsNumber((Float)newValue);
				return;
			case Assignment1Package.COURSE__INSTANCES:
				getInstances().clear();
				getInstances().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case Assignment1Package.COURSE__REQUIRED_COURSES:
				getRequiredCourses().clear();
				getRequiredCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case Assignment1Package.COURSE__RECOMMENDED_COURSES:
				getRecommendedCourses().clear();
				getRecommendedCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case Assignment1Package.COURSE__CONTENT:
				setContent((String)newValue);
				return;
			case Assignment1Package.COURSE__CREDITS_REDUCTION:
				getCreditsReduction().clear();
				getCreditsReduction().addAll((Collection<? extends CourseRelation>)newValue);
				return;
			case Assignment1Package.COURSE__DEPARTMENT:
				setDepartment((Department)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Assignment1Package.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Assignment1Package.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case Assignment1Package.COURSE__CREDITS_NUMBER:
				setCreditsNumber(CREDITS_NUMBER_EDEFAULT);
				return;
			case Assignment1Package.COURSE__INSTANCES:
				getInstances().clear();
				return;
			case Assignment1Package.COURSE__REQUIRED_COURSES:
				getRequiredCourses().clear();
				return;
			case Assignment1Package.COURSE__RECOMMENDED_COURSES:
				getRecommendedCourses().clear();
				return;
			case Assignment1Package.COURSE__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case Assignment1Package.COURSE__CREDITS_REDUCTION:
				getCreditsReduction().clear();
				return;
			case Assignment1Package.COURSE__DEPARTMENT:
				setDepartment((Department)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Assignment1Package.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Assignment1Package.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case Assignment1Package.COURSE__CREDITS_NUMBER:
				return creditsNumber != CREDITS_NUMBER_EDEFAULT;
			case Assignment1Package.COURSE__INSTANCES:
				return instances != null && !instances.isEmpty();
			case Assignment1Package.COURSE__REQUIRED_COURSES:
				return requiredCourses != null && !requiredCourses.isEmpty();
			case Assignment1Package.COURSE__RECOMMENDED_COURSES:
				return recommendedCourses != null && !recommendedCourses.isEmpty();
			case Assignment1Package.COURSE__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case Assignment1Package.COURSE__CREDITS_REDUCTION:
				return creditsReduction != null && !creditsReduction.isEmpty();
			case Assignment1Package.COURSE__DEPARTMENT:
				return getDepartment() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", code: ");
		result.append(code);
		result.append(", creditsNumber: ");
		result.append(creditsNumber);
		result.append(", content: ");
		result.append(content);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
