/**
 */
package assignment1;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see assignment1.Assignment1Factory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface Assignment1Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "assignment1";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.assignment1/model/assignment1.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "assignment1";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Assignment1Package eINSTANCE = assignment1.impl.Assignment1PackageImpl.init();

	/**
	 * The meta object id for the '{@link assignment1.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.CourseImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 1;

	/**
	 * The feature id for the '<em><b>Credits Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS_NUMBER = 2;

	/**
	 * The feature id for the '<em><b>Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__INSTANCES = 3;

	/**
	 * The feature id for the '<em><b>Required Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__REQUIRED_COURSES = 4;

	/**
	 * The feature id for the '<em><b>Recommended Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RECOMMENDED_COURSES = 5;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = 6;

	/**
	 * The feature id for the '<em><b>Credits Reduction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS_REDUCTION = 7;

	/**
	 * The feature id for the '<em><b>Department</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DEPARTMENT = 8;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.CourseRelationImpl <em>Course Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.CourseRelationImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getCourseRelation()
	 * @generated
	 */
	int COURSE_RELATION = 1;

	/**
	 * The feature id for the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION__COURSE = 0;

	/**
	 * The feature id for the '<em><b>Credits Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION__CREDITS_REDUCTION = 1;

	/**
	 * The number of structural features of the '<em>Course Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.CourseInstanceImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 2;

	/**
	 * The feature id for the '<em><b>Evaluation Form</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__EVALUATION_FORM = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__YEAR = 1;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__SEMESTER = 2;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LAB_HOURS = 3;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__LECTURE_HOURS = 4;

	/**
	 * The feature id for the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE_WORK = 5;

	/**
	 * The feature id for the '<em><b>Staff</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__STAFF = 6;

	/**
	 * The feature id for the '<em><b>Study Programs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__STUDY_PROGRAMS = 7;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__COURSE = 8;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.CourseWorkImpl <em>Course Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.CourseWorkImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getCourseWork()
	 * @generated
	 */
	int COURSE_WORK = 3;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LAB_HOURS = 1;

	/**
	 * The number of structural features of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.RoleImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__TYPE = 0;

	/**
	 * The feature id for the '<em><b>People</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__PEOPLE = 1;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.PeopleImpl <em>People</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.PeopleImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getPeople()
	 * @generated
	 */
	int PEOPLE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__EMAIL = 1;

	/**
	 * The number of structural features of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.StudyProgramImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 6;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CODE = 0;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.WorkImpl <em>Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.WorkImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getWork()
	 * @generated
	 */
	int WORK = 7;

	/**
	 * The feature id for the '<em><b>Percentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK__PERCENTAGE = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.DepartmentImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = 1;

	/**
	 * The feature id for the '<em><b>Staff</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__STAFF = 2;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.impl.ScheduledHourImpl <em>Scheduled Hour</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.impl.ScheduledHourImpl
	 * @see assignment1.impl.Assignment1PackageImpl#getScheduledHour()
	 * @generated
	 */
	int SCHEDULED_HOUR = 9;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__DURATION = 0;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__ROOM = 1;

	/**
	 * The feature id for the '<em><b>Beginning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__BEGINNING = 2;

	/**
	 * The feature id for the '<em><b>Reserved For Program</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__RESERVED_FOR_PROGRAM = 3;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__DAY = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR__TYPE = 5;

	/**
	 * The number of structural features of the '<em>Scheduled Hour</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Scheduled Hour</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_HOUR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link assignment1.Semester <em>Semester</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.Semester
	 * @see assignment1.impl.Assignment1PackageImpl#getSemester()
	 * @generated
	 */
	int SEMESTER = 10;

	/**
	 * The meta object id for the '{@link assignment1.Day <em>Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.Day
	 * @see assignment1.impl.Assignment1PackageImpl#getDay()
	 * @generated
	 */
	int DAY = 11;


	/**
	 * The meta object id for the '{@link assignment1.WorkType <em>Work Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.WorkType
	 * @see assignment1.impl.Assignment1PackageImpl#getWorkType()
	 * @generated
	 */
	int WORK_TYPE = 12;

	/**
	 * The meta object id for the '{@link assignment1.HourType <em>Hour Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see assignment1.HourType
	 * @see assignment1.impl.Assignment1PackageImpl#getHourType()
	 * @generated
	 */
	int HOUR_TYPE = 13;


	/**
	 * Returns the meta object for class '{@link assignment1.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see assignment1.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see assignment1.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see assignment1.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Course#getCreditsNumber <em>Credits Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits Number</em>'.
	 * @see assignment1.Course#getCreditsNumber()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_CreditsNumber();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.Course#getInstances <em>Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instances</em>'.
	 * @see assignment1.Course#getInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Instances();

	/**
	 * Returns the meta object for the reference list '{@link assignment1.Course#getRequiredCourses <em>Required Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Courses</em>'.
	 * @see assignment1.Course#getRequiredCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_RequiredCourses();

	/**
	 * Returns the meta object for the reference list '{@link assignment1.Course#getRecommendedCourses <em>Recommended Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Recommended Courses</em>'.
	 * @see assignment1.Course#getRecommendedCourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_RecommendedCourses();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see assignment1.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.Course#getCreditsReduction <em>Credits Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Credits Reduction</em>'.
	 * @see assignment1.Course#getCreditsReduction()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CreditsReduction();

	/**
	 * Returns the meta object for the container reference '{@link assignment1.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Department</em>'.
	 * @see assignment1.Course#getDepartment()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Department();

	/**
	 * Returns the meta object for class '{@link assignment1.CourseRelation <em>Course Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Relation</em>'.
	 * @see assignment1.CourseRelation
	 * @generated
	 */
	EClass getCourseRelation();

	/**
	 * Returns the meta object for the reference '{@link assignment1.CourseRelation#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Course</em>'.
	 * @see assignment1.CourseRelation#getCourse()
	 * @see #getCourseRelation()
	 * @generated
	 */
	EReference getCourseRelation_Course();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.CourseRelation#getCreditsReduction <em>Credits Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits Reduction</em>'.
	 * @see assignment1.CourseRelation#getCreditsReduction()
	 * @see #getCourseRelation()
	 * @generated
	 */
	EAttribute getCourseRelation_CreditsReduction();

	/**
	 * Returns the meta object for class '{@link assignment1.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see assignment1.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.CourseInstance#getEvaluationForm <em>Evaluation Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Evaluation Form</em>'.
	 * @see assignment1.CourseInstance#getEvaluationForm()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_EvaluationForm();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.CourseInstance#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see assignment1.CourseInstance#getYear()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Year();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.CourseInstance#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see assignment1.CourseInstance#getSemester()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Semester();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.CourseInstance#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lab Hours</em>'.
	 * @see assignment1.CourseInstance#getLabHours()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_LabHours();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.CourseInstance#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lecture Hours</em>'.
	 * @see assignment1.CourseInstance#getLectureHours()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_LectureHours();

	/**
	 * Returns the meta object for the containment reference '{@link assignment1.CourseInstance#getCourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Course Work</em>'.
	 * @see assignment1.CourseInstance#getCourseWork()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_CourseWork();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.CourseInstance#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Staff</em>'.
	 * @see assignment1.CourseInstance#getStaff()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Staff();

	/**
	 * Returns the meta object for the reference list '{@link assignment1.CourseInstance#getStudyPrograms <em>Study Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Study Programs</em>'.
	 * @see assignment1.CourseInstance#getStudyPrograms()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_StudyPrograms();

	/**
	 * Returns the meta object for the container reference '{@link assignment1.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see assignment1.CourseInstance#getCourse()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Course();

	/**
	 * Returns the meta object for class '{@link assignment1.CourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Work</em>'.
	 * @see assignment1.CourseWork
	 * @generated
	 */
	EClass getCourseWork();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.CourseWork#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see assignment1.CourseWork#getLectureHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.CourseWork#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see assignment1.CourseWork#getLabHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LabHours();

	/**
	 * Returns the meta object for class '{@link assignment1.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see assignment1.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Role#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see assignment1.Role#getType()
	 * @see #getRole()
	 * @generated
	 */
	EAttribute getRole_Type();

	/**
	 * Returns the meta object for the reference '{@link assignment1.Role#getPeople <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>People</em>'.
	 * @see assignment1.Role#getPeople()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_People();

	/**
	 * Returns the meta object for class '{@link assignment1.People <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>People</em>'.
	 * @see assignment1.People
	 * @generated
	 */
	EClass getPeople();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.People#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see assignment1.People#getName()
	 * @see #getPeople()
	 * @generated
	 */
	EAttribute getPeople_Name();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.People#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see assignment1.People#getEmail()
	 * @see #getPeople()
	 * @generated
	 */
	EAttribute getPeople_Email();

	/**
	 * Returns the meta object for class '{@link assignment1.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see assignment1.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.StudyProgram#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see assignment1.StudyProgram#getCode()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Code();

	/**
	 * Returns the meta object for class '{@link assignment1.Work <em>Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Work</em>'.
	 * @see assignment1.Work
	 * @generated
	 */
	EClass getWork();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Work#getPercentage <em>Percentage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Percentage</em>'.
	 * @see assignment1.Work#getPercentage()
	 * @see #getWork()
	 * @generated
	 */
	EAttribute getWork_Percentage();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Work#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see assignment1.Work#getType()
	 * @see #getWork()
	 * @generated
	 */
	EAttribute getWork_Type();

	/**
	 * Returns the meta object for class '{@link assignment1.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see assignment1.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see assignment1.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see assignment1.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link assignment1.Department#getStaff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Staff</em>'.
	 * @see assignment1.Department#getStaff()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Staff();

	/**
	 * Returns the meta object for class '{@link assignment1.ScheduledHour <em>Scheduled Hour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduled Hour</em>'.
	 * @see assignment1.ScheduledHour
	 * @generated
	 */
	EClass getScheduledHour();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.ScheduledHour#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see assignment1.ScheduledHour#getDuration()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Duration();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.ScheduledHour#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see assignment1.ScheduledHour#getRoom()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Room();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.ScheduledHour#getBeginning <em>Beginning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Beginning</em>'.
	 * @see assignment1.ScheduledHour#getBeginning()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Beginning();

	/**
	 * Returns the meta object for the reference list '{@link assignment1.ScheduledHour#getReservedForProgram <em>Reserved For Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reserved For Program</em>'.
	 * @see assignment1.ScheduledHour#getReservedForProgram()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EReference getScheduledHour_ReservedForProgram();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.ScheduledHour#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see assignment1.ScheduledHour#getDay()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Day();

	/**
	 * Returns the meta object for the attribute '{@link assignment1.ScheduledHour#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see assignment1.ScheduledHour#getType()
	 * @see #getScheduledHour()
	 * @generated
	 */
	EAttribute getScheduledHour_Type();

	/**
	 * Returns the meta object for enum '{@link assignment1.Semester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Semester</em>'.
	 * @see assignment1.Semester
	 * @generated
	 */
	EEnum getSemester();

	/**
	 * Returns the meta object for enum '{@link assignment1.Day <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day</em>'.
	 * @see assignment1.Day
	 * @generated
	 */
	EEnum getDay();

	/**
	 * Returns the meta object for enum '{@link assignment1.WorkType <em>Work Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Work Type</em>'.
	 * @see assignment1.WorkType
	 * @generated
	 */
	EEnum getWorkType();

	/**
	 * Returns the meta object for enum '{@link assignment1.HourType <em>Hour Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Hour Type</em>'.
	 * @see assignment1.HourType
	 * @generated
	 */
	EEnum getHourType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Assignment1Factory getAssignment1Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link assignment1.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.CourseImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Credits Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS_NUMBER = eINSTANCE.getCourse_CreditsNumber();

		/**
		 * The meta object literal for the '<em><b>Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__INSTANCES = eINSTANCE.getCourse_Instances();

		/**
		 * The meta object literal for the '<em><b>Required Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__REQUIRED_COURSES = eINSTANCE.getCourse_RequiredCourses();

		/**
		 * The meta object literal for the '<em><b>Recommended Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RECOMMENDED_COURSES = eINSTANCE.getCourse_RecommendedCourses();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Credits Reduction</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__CREDITS_REDUCTION = eINSTANCE.getCourse_CreditsReduction();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__DEPARTMENT = eINSTANCE.getCourse_Department();

		/**
		 * The meta object literal for the '{@link assignment1.impl.CourseRelationImpl <em>Course Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.CourseRelationImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getCourseRelation()
		 * @generated
		 */
		EClass COURSE_RELATION = eINSTANCE.getCourseRelation();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_RELATION__COURSE = eINSTANCE.getCourseRelation_Course();

		/**
		 * The meta object literal for the '<em><b>Credits Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_RELATION__CREDITS_REDUCTION = eINSTANCE.getCourseRelation_CreditsReduction();

		/**
		 * The meta object literal for the '{@link assignment1.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.CourseInstanceImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Evaluation Form</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__EVALUATION_FORM = eINSTANCE.getCourseInstance_EvaluationForm();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__YEAR = eINSTANCE.getCourseInstance_Year();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__SEMESTER = eINSTANCE.getCourseInstance_Semester();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__LAB_HOURS = eINSTANCE.getCourseInstance_LabHours();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__LECTURE_HOURS = eINSTANCE.getCourseInstance_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Course Work</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE_WORK = eINSTANCE.getCourseInstance_CourseWork();

		/**
		 * The meta object literal for the '<em><b>Staff</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__STAFF = eINSTANCE.getCourseInstance_Staff();

		/**
		 * The meta object literal for the '<em><b>Study Programs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__STUDY_PROGRAMS = eINSTANCE.getCourseInstance_StudyPrograms();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__COURSE = eINSTANCE.getCourseInstance_Course();

		/**
		 * The meta object literal for the '{@link assignment1.impl.CourseWorkImpl <em>Course Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.CourseWorkImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getCourseWork()
		 * @generated
		 */
		EClass COURSE_WORK = eINSTANCE.getCourseWork();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LECTURE_HOURS = eINSTANCE.getCourseWork_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LAB_HOURS = eINSTANCE.getCourseWork_LabHours();

		/**
		 * The meta object literal for the '{@link assignment1.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.RoleImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE__TYPE = eINSTANCE.getRole_Type();

		/**
		 * The meta object literal for the '<em><b>People</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__PEOPLE = eINSTANCE.getRole_People();

		/**
		 * The meta object literal for the '{@link assignment1.impl.PeopleImpl <em>People</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.PeopleImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getPeople()
		 * @generated
		 */
		EClass PEOPLE = eINSTANCE.getPeople();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PEOPLE__NAME = eINSTANCE.getPeople_Name();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PEOPLE__EMAIL = eINSTANCE.getPeople_Email();

		/**
		 * The meta object literal for the '{@link assignment1.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.StudyProgramImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__CODE = eINSTANCE.getStudyProgram_Code();

		/**
		 * The meta object literal for the '{@link assignment1.impl.WorkImpl <em>Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.WorkImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getWork()
		 * @generated
		 */
		EClass WORK = eINSTANCE.getWork();

		/**
		 * The meta object literal for the '<em><b>Percentage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORK__PERCENTAGE = eINSTANCE.getWork_Percentage();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORK__TYPE = eINSTANCE.getWork_Type();

		/**
		 * The meta object literal for the '{@link assignment1.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.DepartmentImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Staff</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__STAFF = eINSTANCE.getDepartment_Staff();

		/**
		 * The meta object literal for the '{@link assignment1.impl.ScheduledHourImpl <em>Scheduled Hour</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.impl.ScheduledHourImpl
		 * @see assignment1.impl.Assignment1PackageImpl#getScheduledHour()
		 * @generated
		 */
		EClass SCHEDULED_HOUR = eINSTANCE.getScheduledHour();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__DURATION = eINSTANCE.getScheduledHour_Duration();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__ROOM = eINSTANCE.getScheduledHour_Room();

		/**
		 * The meta object literal for the '<em><b>Beginning</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__BEGINNING = eINSTANCE.getScheduledHour_Beginning();

		/**
		 * The meta object literal for the '<em><b>Reserved For Program</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULED_HOUR__RESERVED_FOR_PROGRAM = eINSTANCE.getScheduledHour_ReservedForProgram();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__DAY = eINSTANCE.getScheduledHour_Day();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_HOUR__TYPE = eINSTANCE.getScheduledHour_Type();

		/**
		 * The meta object literal for the '{@link assignment1.Semester <em>Semester</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.Semester
		 * @see assignment1.impl.Assignment1PackageImpl#getSemester()
		 * @generated
		 */
		EEnum SEMESTER = eINSTANCE.getSemester();

		/**
		 * The meta object literal for the '{@link assignment1.Day <em>Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.Day
		 * @see assignment1.impl.Assignment1PackageImpl#getDay()
		 * @generated
		 */
		EEnum DAY = eINSTANCE.getDay();

		/**
		 * The meta object literal for the '{@link assignment1.WorkType <em>Work Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.WorkType
		 * @see assignment1.impl.Assignment1PackageImpl#getWorkType()
		 * @generated
		 */
		EEnum WORK_TYPE = eINSTANCE.getWorkType();

		/**
		 * The meta object literal for the '{@link assignment1.HourType <em>Hour Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see assignment1.HourType
		 * @see assignment1.impl.Assignment1PackageImpl#getHourType()
		 * @generated
		 */
		EEnum HOUR_TYPE = eINSTANCE.getHourType();

	}

} //Assignment1Package
