/**
 */
package assignment1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exam</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see assignment1.Assignment1Package#getExam()
 * @model
 * @generated
 */
public interface Exam extends Work {
} // Exam
