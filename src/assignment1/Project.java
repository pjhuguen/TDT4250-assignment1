/**
 */
package assignment1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see assignment1.Assignment1Package#getProject()
 * @model
 * @generated
 */
public interface Project extends Work {
} // Project
